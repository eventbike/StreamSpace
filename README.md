# StreamSpace

This project was more of an experiment and is not continuously developed, but extended and / or maintained on demand. The goal is to embed livestreams from third-party sites into a simple web site.

The project was created by the eventbike crew for SpaceLeaves as the project "StreamSpace". Work was transfered in 2021 to the eventbike org. We're open to respond to your issues, but you'll most likely see not much documentation or progress here.


This repository is our first try on [GatsbyJS](https://www.gatsbyjs.org/)

## Installation, Development, Deployment
- get GatsbyJS from their official website
- run npm install 
- place your stream config in streams/
- run `gatsby serve` or `gatsby build`
