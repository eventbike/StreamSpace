module.exports = {
	/* Your site config here */
	siteMetadata: {
			title: `StreamSpace`,
			description: `eventbike_zero is live!`
	},
	plugins: [
			{
			resolve: `gatsby-source-filesystem`,
			options: {
				name: `src`,
				path: `${__dirname}/streams/`,
			},
		},
		`gatsby-plugin-remove-trailing-slashes`,
		`gatsby-transformer-remark`,
	],
}
